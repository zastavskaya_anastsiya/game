package ru.zak.game;

/**
 * Класс для запуска игры GuessGame
 * @author Заставская
 */
public class GameLauncher {
    public static void main (String[] args){
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
