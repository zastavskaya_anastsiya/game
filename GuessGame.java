package ru.zak.game;

/**
 * Класс с игрой
 *
 */
public class GuessGame {
    Player playerOne;
    Player playerTwo;
    Player playerThree;
    public void startGame(){
        playerOne = new Player();
        playerTwo = new Player();
        playerThree = new Player();
        int guesspPlayerOne = 0;
        int guesspPlyerTwo = 0;
        int guesspPlayerThree = 0;
        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;
        int targetNumber = (int) (Math.random()*10);
        System.out.println("Я думаю о числе между 0 и 9...");
        while (true){
            System.out.println("Загаданое чило "+ targetNumber);

            playerOne.guess();
            playerTwo.guess();
            playerThree.guess();

            guesspPlayerOne = playerOne.number;
            System.out.println("Игрок первый угадал " + guesspPlayerOne);
            guesspPlyerTwo = playerTwo.number;
            System.out.println("Игрок второй угадал "+ guesspPlyerTwo);
            guesspPlayerThree = playerThree.number;
            System.out.println("Игрок третий угадал "+guesspPlayerThree);

            if(guesspPlayerOne == targetNumber){
                p1isRight = true;
            }
            if(guesspPlyerTwo == targetNumber){
                p2isRight = true;
            }
            if(guesspPlayerThree == targetNumber){
                p3isRight = true;
            }
            if(p1isRight || p2isRight||p3isRight)
            {
                System.out.println("У нас есть победитель!");
                System.out.println("Игрок первый прав? "+p1isRight);
                System.out.println("Игрок второй понял? "+p2isRight);
                System.out.println("Игрок третий ? "+p3isRight);

                System.out.println("Игра оконченна");
                break;
            }
            else {
                System.out.println("верного ответа не прозвучало.Игрокам придёться попробывать снова.");
            }
        }
    }
}
